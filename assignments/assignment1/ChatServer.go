/*! *****************************************************************************
Copyright (c) Ruthvik Kolli. All rights reserved.
***************************************************************************** */

/* Simple EchoServer in GoLang by Phu Phung, customized by <Ruthvik> for SecAD*/
package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const BUFFERSIZE int = 1024

var allClient_conns = make(map[net.Conn]string)
var newclient = make(chan net.Conn)
var lostClient = make(chan net.Conn)
var allLoggedIn_conns = make(map[net.Conn]interface{})

type User struct {
	Username string
	Login    bool
	Key      string
	Name     string
}

type Message struct {
	Username string
	Password string
	Option   string
	Fullname string
}

// const menu = "menu.action Broadcast to all, chat with user, Broadcast to only logged-in users, see who is active"

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s <port>\n", os.Args[0])
		os.Exit(0)
	}
	port := os.Args[1]
	if len(port) > 5 {
		fmt.Println("Invalid port value. Try again!")
		os.Exit(1)
	}
	server, erro := net.Listen("tcp", ":"+port)
	if erro != nil {
		fmt.Printf("Cannot listen on port '" + port + "'!\n")
		os.Exit(2)
	}
	fmt.Println("EchoServer in GoLang developed by Ruthvik Kolli")
	fmt.Printf("EchoServer is listening on port '%s' ...\n", port)
	connectDb()
	go func() {
		for {
			client_conn, _ := server.Accept()

			// isVerified :=
			// login(client_conn)
			// if isVerified {
			// some code
			newclient <- client_conn
			// }

		}
	}()

	for {
		select {
		case client_conn := <-newclient:
			allClient_conns[client_conn] = client_conn.RemoteAddr().String()
			fmt.Printf("A new client '%s' connected!\n# of connected clients: %d\n ", client_conn.RemoteAddr().String(), len(allClient_conns))

			go client_goroutine(client_conn)

		case client_conn := <-lostClient:
			//handling for the event
			delete(allClient_conns, client_conn)
			delete(allLoggedIn_conns, client_conn)
			// byemessage := fmt.Sprintf("Client '%s' is DISCONNECTED!\n# of connected clients: %d\n", client_conn.RemoteAddr().String(), len(allClient_conns))
			// sendtoAll(byemessage)
		}
	}
}

func getInput(client_conn net.Conn) (string, error) {
	var buffer [BUFFERSIZE]byte
	byte_received, read_err := client_conn.Read(buffer[0:])
	recv := toString(buffer[0:byte_received])
	fmt.Println("DEBUG>RECV received Data: " + recv)
	if read_err != nil {
		fmt.Println("Error in receiving...")
		lostClient <- client_conn
		return "", read_err
	}

	return recv, nil
}

func client_goroutine(client_conn net.Conn) {
	go func() {
		var userName string
		for {
			recv, err := getInput(client_conn)

			if err != nil {
				return
			}
			if strings.Contains(recv, ".echo") {
				sendto(client_conn, recv, userName)
			} else if strings.Contains(recv, ".help") {
				var options string
				defer recoverFromNoHelpPanic(client_conn)
				if allLoggedIn_conns[client_conn].(User).Login {

					options = " .logout to logout \n"
				} else {
					options = " .login to login \n .signup to signup \n"
				}
				sendto(client_conn, "\n Type 'users' to get users \n Type 'sendAll [message]' to send message to all \n Type 'sendLogged [message]' to send to Logged users \n Type 'to:[name] [message]' to send to specific user \n\n .exit to exit \n"+options, "System")
			} else if strings.Contains(recv, "login") || strings.Contains(recv, "signup") {
				_, userName = login(client_conn, recv)
			} else if strings.Contains(recv, "users") {
				userslist := getActiveUsersList(client_conn)
				sendto(client_conn, "Active Users: \n"+strings.Join(userslist, "\n"), "System")
			} else if strings.Contains(recv, "sendAll") {
				message := strings.Split(recv, "sendAll ")
				sendtoAll(message[1], userName)
			} else if strings.Contains(recv, "sendLogged") {
				message := strings.Split(recv, "sendLogged ")
				sendtoLogged(message[1], userName)
			} else if strings.Contains(recv, "to:") {
				user := strings.Split(strings.Split(recv, ":")[1], " ")[0]
				message := strings.Join(strings.Split(recv, " ")[1:], " ")
				fmt.Println(user)
				sendtoUser(user, message, userName, client_conn)
			} else {
				sendto(client_conn, "Incorrect Format of Data input.\n type .help for help", userName)
			}
			// if strings.Contains(recv, ".menu") {
			// 	// code here
			// 	if strings.Contains(recv, ".menu0") {
			// 		fmt.Println("Menu option selected 1 Broadcast to all")
			// 		sendto(client_conn, "keyboard.action Enter message to send: ")
			// 		message, err := getInput(client_conn)
			// 		if err != nil {
			// 			fmt.Println("Debug> error")
			// 			return
			// 		}
			// 		sendtoAll(message)
			// 		continue
			// 	} else if strings.Contains(recv, ".menu1") {
			// 		sendActiveUsersList(client_conn)
			// 	} else if strings.Contains(recv, ".menu2") {
			// 		message, err := getInput(client_conn)
			// 		if err != nil {
			// 			return
			// 		}
			// 		sendtoLogged(message)
			// 	} else if strings.Contains(recv, ".menu3") {
			// 		sendActiveUsersList(client_conn)
			// 	}
			// } else {
			// 	sendto(client_conn, recv)
			// }
		}
	}()
}

func getUser(client net.Conn) (string, error) {
	defer func() string {
		if r := recover(); r != nil {
			str := fmt.Sprintln("Recovered", r)
			return str
		}
		return ""
	}()
	return allLoggedIn_conns[client].(User).Username, nil
}

func getActiveUsersList(client_Conn net.Conn) []string {
	defer recoverFromNoUserPanic(client_Conn)
	fmt.Println("Preparing list of users")
	var userslist []string
	for i, v := range allClient_conns {
		user, _ := getUser(i)
		userslist = append(userslist, user)
		fmt.Println(v)
	}
	// sendto(client_Conn, "Active Users: \n"+strings.Join(userslist, "\n"), "System")
	return userslist
}

func sendtoLogged(data string, from string) {
	for client_conn := range allLoggedIn_conns {
		sendto(client_conn, data, from)
	}
	fmt.Printf("Send data to all clients!: %s ", data)
}

func sendtoAll(data string, from string) {
	if from == "" {
		for client_conn := range allClient_conns {
			if allLoggedIn_conns[client_conn] == nil {
				sendto(client_conn, data, from)
			}
		}
	} else {

		// fmt.Println("DEBUG>Send To ALL")
		for client_conn := range allClient_conns {
			sendto(client_conn, data, from)
		}
		fmt.Printf("Send data to all clients!: %s ", data)
	}
}

func sendtoUser(username string, data string, from string, client net.Conn) {
	if from == "" {
		sendto(client, "Login first to send message to this user", "System")
	} else {
		for client_conn := range allClient_conns {
			if allLoggedIn_conns[client_conn].(User).Username == username {
				sendto(client_conn, data, from)
			}
		}
	}
}

func sendto(client_conn net.Conn, data string, from string) {
	if from == "" {
		_, write_err := client_conn.Write([]byte(data))
		if write_err != nil {
			fmt.Println("Error in sending...")
			return
		}
	} else {
		_, write_err := client_conn.Write([]byte("From: " + from + "; Message: " + data))
		if write_err != nil {
			fmt.Println("Error in sending...")
			return
		}
	}
	// fmt.Printf("Sending! '%s' \n ", data)
}

func parseJson(data string) (Message, string, error) {
	var message Message
	err := json.Unmarshal([]byte(data), &message)
	if err != nil {
		fmt.Printf("JSON parsing error: '%s' \n", err)
		return message, `exit.action [BAD LOGIN] Expected: {"Username":"...","Password":"..."}`, err
	}
	return message, "Login Success", nil
}

func recoverFromDbPanic(client_conn net.Conn) {
	if r := recover(); r != nil {
		str := fmt.Sprintln("Recovered", r)
		if strings.Contains(str, "no documents in result") {
			sendto(client_conn, "No user Found with qiven credentials", "Database")
		} else {
			sendto(client_conn, "No user found or error wihile retrieving data", "Database")
		}
	}
}

func recoverFromNoHelpPanic(client_conn net.Conn) {
	if r := recover(); r != nil {
		// str := fmt.Sprintln("Recovered", r)
		sendto(client_conn, "\n Type 'users' to get users \n Type 'sendAll [message]' to send message to all \n Type 'sendLogged [message]' to send to Logged users \n Type 'to:[name] [message]' to send to specific user \n\n .exit to exit \n", "System")

	}
}

func recoverFromNoUserPanic(client_conn net.Conn) {
	if r := recover(); r != nil {
		str := fmt.Sprintln("Recovered", r)
		sendto(client_conn, "No Loggedin User Found err:"+str, "System")

	}
}

func login(client_conn net.Conn, recv string) (bool, string) {
	defer recoverFromDbPanic(client_conn)
	// get username and password using API
	// recv, err := getInput(client_conn)
	// if err != nil {
	// 	return false
	// }
	json, message, err := parseJson(recv)
	if err != nil {
		sendto(client_conn, message, "System")
		client_conn.Close()
		return false, ""
	}
	if strings.Contains(recv, "login") {
		verified, username, fullname, message := checkLogin(json)
		if verified {
			sendto(client_conn, "Welcome "+fullname, "System")
			welcomeMessage := fmt.Sprintf("New user %s, connected and updated user list is", fullname)
			sendtoLogged(welcomeMessage, "System")

			connectedUser := User{Name: fullname, Login: true, Username: username}
			allLoggedIn_conns[client_conn] = connectedUser
			// sendto(client_conn, menu)
			// newclient <- client_conn

			userslist := getActiveUsersList(client_conn)
			sendtoLogged("Active Users: \n"+strings.Join(userslist, "\n"), "System")

		} else {
			sendto(client_conn, message, "System")
		}
		if username != "" {
			return true, username
		} else {
			return false, ""
		}
	} else if strings.Contains(json.Option, "signup") {
		res := signUp(json)
		if res != nil {
			// sendto(client_conn, "keyboard.action You have successfully signed up and logged in")
			// sendto(client_conn, menu)
			newclient <- client_conn

		} else {
			sendto(client_conn, "Unable to sign you up right now, Please try again later", "System")
		}
		return true, json.Username
	}
	return false, ""
}

func toString(data []byte) string {
	return string(data)
}

func signUp(message Message) *mongo.InsertOneResult {
	// refer document for database guidelines
	// https://www.mongodb.com/blog/post/mongodb-go-driver-tutorial-part-1-connecting-using-bson-and-crud-operations

	_, usersCollection := connectDb()

	// insert a single document into a collection
	// create a bson.D object
	user := bson.D{{Key: "fullName", Value: message.Fullname}, {Key: "password", Value: message.Password}, {Key: "username", Value: message.Username}}
	// insert the bson object using InsertOne()
	result, err := usersCollection.InsertOne(context.TODO(), user)

	// check for errors in the insertion
	if err != nil {
		panic(err)
	}
	// display the id of the newly inserted object
	fmt.Println(result.InsertedID)
	return result
}

func checkLogin(data Message) (bool, string, string, string) {
	//signUp("Ruthvik", username, password)

	fmt.Printf("DEBUG> GOT:account = %s\n", data)

	login, name := checkAccount(data.Username, data.Password)
	if login {

		return login, data.Username, name, "Logged IN"
	}
	return false, "", "", "Invalid username or password\n"
}

func connectDb() (*mongo.Client, *mongo.Collection) {
	serverAPIOptions := options.ServerAPI(options.ServerAPIVersion1)
	clientOptions := options.Client().
		ApplyURI("mongodb+srv://kollir2:kollir2@cca-kollir2.h0h2e.mongodb.net/secad?retryWrites=true&w=majority").
		SetServerAPIOptions(serverAPIOptions)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	usersCollection := client.Database("secad").Collection("users")
	return client, usersCollection
}

func checkAccount(username string, password string) (bool, string) {
	// retrieve single and multiple documents with a specified filter using FindOne() and Find()
	// create a search filer
	_, usersCollection := connectDb()
	filter := bson.D{primitive.E{Key: "username", Value: username}, {Key: "password", Value: password}}

	// retrieve all the documents that match the filter
	cursor, err := usersCollection.Find(context.TODO(), filter)
	// check for errors in the finding
	if err != nil {
		panic(err)
	}

	// convert the cursor result to bson
	var results []bson.M
	// check for errors in the conversion
	if err = cursor.All(context.TODO(), &results); err != nil {
		panic(err)
	}

	// retrieving the first document that match the filter
	var result bson.M
	// check for errors in the finding
	if err = usersCollection.FindOne(context.TODO(), filter).Decode(&result); err != nil {
		panic(err)
	}

	// display the document retrieved
	fmt.Println("Connected user: " + result["fullName"].(string))
	return true, result["fullName"].(string)
}
