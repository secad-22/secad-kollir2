/*! *****************************************************************************
Copyright (c) Ruthvik Kolli. All rights reserved.
***************************************************************************** */

var net = require('net');
const readlineSync = require('readline-sync')

if (process.argv.length != 4) {
    console.log("Usage: node %s <host> <port>", process.argv[1]);
    process.exit(1);
}

var host = process.argv[2];
var port = process.argv[3];

const keyboard = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

if (host.length > 253 || port.length > 5) {
    console.log("Invalid host or port. Try again!\nUsage: node %s <port>", process.argv[1]);
    process.exit(1);
}

var client = new net.Socket();
console.log("Simple telnet.js developed by Ruthvik Kolli, SecAD");
console.log("Connecting to: %s:%s", host, port);

client.connect(port, host, connected);

function connected() {
    console.log("Connected to: %s:%s \n Type .exit to exit\n Type .login to Login \n Type .signup to Signup \n .help for Help", client.remoteAddress, client.remotePort);

}

client.on("data", data => {
    console.log("Receied Data: " + data);
});

client.on("error", function (err) {
    console.log("Error");
    process.exit(2);
});

client.on("close", function (data) {
    console.log("Connection has been disconnected");
    process.exit(3);
});


function signup() {
    var fullname = readlineSync.question('Full Name: ');
    if (!inputValidated(fullname)) {
        console.log("Name must have at least 5 characters. Please try again");
        startMenu()
        return;
    }
    var username = readlineSync.question('Username: ');
    if (!inputValidated(username)) {
        console.log("Username must have at least 5 characters. Please try again");
        startMenu();
        return;
    }
    var password = readlineSync.question('Password: ', { hideEchoBack: true });
    if (!inputValidated(password)) {
        console.log("Password must have at least 5 characters. Please try again!");
        startMenu();
        return;
    }
    var signupMessage = JSON.stringify({ "Option": "signup", "Fullname": fullname, "Username": username, "Password": password });
    client.write(signupMessage);
    keyboard.prompt();
}



function login() {
    username = readlineSync.question('Username: ');
    if (!inputValidated(username)) {
        console.log("Username must have at least 5 characters. Please try again");
        startMenu();
        return;
    }
    password = readlineSync.question('Password: ', { hideEchoBack: true });
    if (!inputValidated(password)) {
        console.log("Password must have at least 5 characters. Please try again!");
        startMenu();
        return;
    }
    var login = JSON.stringify({ "Option": "login", "Username": username, "Password": password, "Fullname": "" });
    client.write(login);
    keyboard.prompt();
}

function inputValidated(data) {
    var str = String(data);
    return str.length > 2

}

keyboard.on('line', (input) => {
    // console.log('You typed: ${input}');
    if (input === ".signup") {
        keyboard.pause();
        signup();
    } else
        if (input === ".login") {
            keyboard.pause();
            login();
        } else if (input === ".exit") {
            client.destroy();
            console.log("disconnected!");
            process.exit();
        } else {
            client.write(input);
        }
});