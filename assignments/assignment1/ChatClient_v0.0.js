/*! *****************************************************************************
Copyright (c) Ruthvik Kolli. All rights reserved. 
***************************************************************************** */

var net = require('net');

if (process.argv.length != 4) {
    console.log("Usage: node %s <host> <port>", process.argv[1]);
    process.exit(1);
}

var host = process.argv[2];
var port = process.argv[3];
const readlineSync = require('readline-sync')
const keyboard = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

if (host.length > 253 || port.length > 5) {
    console.log("Invalid host or port. Try again!\nUsage: node %s <port>", process.argv[1]);
    process.exit(1);
}

var client = new net.Socket();
console.log("Simple telnet.js developed by Phu Phung, SecAD");
console.log("Connecting to: %s:%s", host, port);

client.connect(port, host, connected);

function connected() {
    console.log("Connected to: %s:%s", client.remoteAddress, client.remotePort);
    // startMenu()
}

// function mainMenu(options) {
//     var index = readlineSync.keyInSelect(options, "Please select one option")
//     client.write(".menu" + index);
//     // switch (index){
//     //     case 0: readlineSync.question("Enter message to send")
//     // }
// }

function startMenu() {
    var options = ["Login", "Signup", "exit"]
    var index = readlineSync.keyInSelect(options, "Please select one option")
    switch (index) {
        case 0: login();
            break;
        case 1: signup();
            break;
        case 2: process.exit(1);
    }
}

var username;
var password;

// function signup() {
//     var fullname = readlineSync.question('Full Name: ');
//     if (!inputValidated(fullname)) {
//         console.log("Name must have at least 5 characters. Please try again");
//         startMenu()
//         return;
//     }
//     var username = readlineSync.question('Username: ');
//     if (!inputValidated(username)) {
//         console.log("Username must have at least 5 characters. Please try again");
//         startMenu();
//         return;
//     }
//     var password = readlineSync.question('Password: ', { hideEchoBack: true });
//     if (!inputValidated(password)) {
//         console.log("Password must have at least 5 characters. Please try again!");
//         startMenu();
//         return;
//     }
//     var signupMessage = JSON.stringify({ "Option": "signup", "Fullname": fullname, "Username": username, "Password": password });
//     client.write(signupMessage);
// }



// function login() {
//     username = readlineSync.question('Username: ');
//     if (!inputValidated(username)) {
//         console.log("Username must have at least 5 characters. Please try again");
//         startMenu();
//         return;
//     }
//     password = readlineSync.question('Password: ', { hideEchoBack: true });
//     if (!inputValidated(password)) {
//         console.log("Password must have at least 5 characters. Please try again!");
//         startMenu();
//         return;
//     }
//     var login = JSON.stringify({ "Option": "login", "Username": username, "Password": password, "Fullname": "" });
//     client.write(login);
// }
function inputValidated(data) {
    var str = String(data);
    return str.length > 2

}
// function parseMessage(data) {
//     var str = String(data);
//     if (str.includes(".action")) {
//         let strs = str.split(".action ");
//         switch (strs[0]) {
//             case 'menu':
//                 mainMenu(strs[1].split(", "));
//                 return "";
//             case 'keyboard':
//                 // keyboard.on('line', (input) => {
//                 //     console.log('You typed: ${input}');
//                 //     if (input === ".exit") {
//                 //         client.destroy();
//                 //         console.log("disconnected!");
//                 //         process.exit();
//                 //     } else {
//                 //         console.log("Sending data to server");
//                 //         client.write(input);
//                 //     }
//                 // });
//                 console.log("Type .exit to exit");
//                 do {
//                     message = readlineSync.question("Enter message to send: ");
//                     if (message == ".exit") {
//                         client.destroy();
//                         console.log("disconnected!");
//                         process.exit();
//                     }
//                     client.write(message);
//                 } while (message == ".exit");

//                 return "";
//             case 'exit': process.exit(3);
//             default: console.log(str[0]);
//                 return "";
//         }
//     }
//     console.log("Receied Data: " + data);
//     return data
// }


client.on("data", data => {
    // message = parseMessage(data)
    console.log("You Recived: " + data);
    // sendMessage();
});

client.on("error", function (err) {
    console.log("Error");
    process.exit(2);
});

client.on("close", function (data) {
    console.log("Connection has been disconnected");
    process.exit(3);
});

// function sendMessage() {
//     input = readlineSync.question("Enter Choice: ");
//     if (input === ".exit") {
//         client.destroy();
//         console.log("disconnected!");
//         process.exit();
//     } else {
//         console.log("Sending data to server");
//         client.write(input);
//     }
    keyboard.on('line', (input) => {
        console.log('You typed: ${input}');
        if (input === ".exit") {
            client.destroy();
            console.log("disconnected!");
            process.exit();
        } else {
            console.log("Sending data to server");
            client.write(input);
        }
    });
// }

