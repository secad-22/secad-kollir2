#!/bin/bash
program=$1
#some statements
count = 1000
echo "Detecting max input size by Ruthvik"; pwd

#loop
while true
do
let count+=1	
$program $(perl -e "print 'a'x$count")
return_code=$?;

if [[ $return_code -eq 127 ]]; then
#some statements

echo "Program is not found"

elif [ $return_code -eq 0 ]
then
echo "Executed successfully with input length = $count"

#some statements
else
#some statement
echo "Execution failed at input length = $count and max input for the program is $(($count - 1 ))"
break;
fi
done
#end loop
