/* include libraries */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>


int main (int argc, char *argv[])
{
	if (argc!=3)
	{
		/* code */
		printf("Usage: %s <servername> <path>\n",argv[0]);
		exit(1);
	}
   printf("TCP Client program by Ruthvik Kolli\n");


   char *servername = argv[1];
   char *path = argv[2];
   if(strlen(servername) > 255 || strlen(path) > 255){
   	printf("Servername or path is too long. Please try again!\n");
   	exit(2);
   }

   printf("Servername= %s,  path= %s\n",servername,path);

   int sockfd = socket(AF_INET, SOCK_STREAM, 0);
   if(sockfd < 0){
   	perror("Error opening socket");
   	exit(sockfd);
   }
	printf("A socket is opened\n");

	struct addrinfo hints, *serveraddr;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	int addr_lookup = getaddrinfo(servername, "80", &hints, &serveraddr);
	if (addr_lookup != 0) {
		fprintf(stderr, "getaddrinfo: %s\n",gai_strerror(addr_lookup));
		exit(3);
	}
	
	int connected = connect(sockfd, serveraddr->ai_addr, serveraddr->ai_addrlen);
	if(connected < 0){
		perror("Cannot connect to the server\n");
		exit(4);
	}
	printf("Connected to the server %s \n", servername, path);
	
	int BUFFERSIZE = 1024; //define the size of the buffer
	char buffer[BUFFERSIZE]; //define the buffer
	bzero(buffer,BUFFERSIZE);
	printf("Request sent to the server:");
	sprintf(buffer,"GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", path, servername);
	printf("%s",buffer);
	int byte_received = recv(sockfd, buffer, BUFFERSIZE, 0);
	int byte_sent = send(sockfd,buffer, strlen(buffer), 0);

	freeaddrinfo(serveraddr);

	bzero(buffer,BUFFERSIZE);

	
	if(byte_received <0){
		perror("Error in reading");
		exit(4);
	}
	printf("Received from server: %s", buffer);

	close(sockfd);
	return 0;
}
