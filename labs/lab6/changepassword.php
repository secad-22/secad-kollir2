
<?php 
require 'session_auth.php';
require 'database.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Change Password page - SecAD</title>
</head>
<body>
      	<h1>A Simple change password form, SecAD</h1>

<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa");

  if(isset($_REQUEST['password'])){
 	$nocsrftoken = $REQUEST["nocsrftoken"];
    if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
    	echo "<script>alert('Cross-site request forgery is detected!');</script>";
    	header("Refresh:0;url= logout.php");
    	die();
    }

  	if (securechangepassword($_SESSION["username"], $_REQUEST["password"])) {
		
	}else{
		echo "<script>alert('Unable to change password');</script>";
		die();
	}
  } else {
  $rand = bin2hex(openssl_random_pseudo_bytes(16));
  $_SESSION["nocsrftoken"] = $rand;




?>
          <form action="changepassword.php" method="POST" class="form login">
                
                New Password: <input type="password" class="text_field" name="password" /> <br>
                <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>">
                <button class="button" type="submit">
                  Change password
                </button>
          </form>
<?php 
}
?>

<br>

          <a href="index.php">Home</a> | <a href = "logout.php">Logout</a>

</body>
</html>


